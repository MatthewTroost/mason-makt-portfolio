import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    moreDisabled: false,
    leftRightNavDisabled: false,
    previousLeftRightValue: false,
    projectEndReached: false
  },
  mutations: {
    setLeftRightNavDisabled(state, value) {
      state.previousLeftRightValue = state.leftRightNavDisabled;
      state.leftRightNavDisabled = value;
    },
    setprojectEndReached(state, value) {
      state.projectEndReached = value;
    },
    setMoreDisabled(state, value) {
      state.moreDisabled = value;
    }
  },
  getters: {
    leftRightNavDisabled: state => state.leftRightNavDisabled,
    projectEndReached: state => state.projectEndReached,
    previousLeftRightValue: state => state.previousLeftRightValue,
    moreDisabled: state => state.moreDisabled
  }
});
