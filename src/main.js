import Vue from 'vue'
import App from '../src/components/App.vue'
import { store } from './store/store'

import '../node_modules/swiper/css/swiper.min.css'
import './styles/custom.css'

import '../node_modules/swiper/js/swiper.js'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  store
}).$mount('#app')
